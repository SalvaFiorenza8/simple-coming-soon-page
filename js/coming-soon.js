/* Copyright (c) 2016 Salvatore Fiorenza
   license: MIT
   contact: salvatore.fiorenza.8@gmail.com */

if (typeof jQuery === "undefined") {
  throw new Error("This app requires jQuery");
}

const appClock = {
    endDate: new Date(2017, 10, 14),

    getDaysHoursMinutesSecondsLeft: function (startDate, endDate) {
        var totalDiff = Math.abs(startDate - endDate);
        var daysLeft = totalDiff / (1000 * 60 * 60* 24);
        var hoursLeft = (daysLeft % 1) * 24;
        var minutesLeft = (hoursLeft % 1) * 60;
        var secondsLeft = (minutesLeft % 1) * 60;
        return {
            daysLeft: Math.floor(daysLeft),
            hoursLeft: Math.floor(hoursLeft),
            minutesLeft: Math.floor(minutesLeft),
            secondsLeft: Math.floor(secondsLeft)
        }

    },
            
    init: function () {
        Date.prototype.clone = function() { return new Date(this.getTime()); };
        this.daysTag = jQuery("#daysLeft");
        this.hoursTag = jQuery("#hoursLeft");
        this.minutesTag = jQuery("#minutesLeft");
        this.secondsTag = jQuery("#secondsLeft");
        this.updateTagsValues();
        this.daemon = window.setInterval(this.updateTagsValues, 1000);
    },

    randomDate: function (start, end) {
        return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
    },
            
    updateTagsValues: function () {
        if (appClock.timeLeft === undefined) {
            var startDate = new Date();
            appClock.timeLeft = appClock.getDaysHoursMinutesSecondsLeft(startDate, appClock.endDate);
        }
        else {
            timeLeft = appClock.timeLeft;
            if (timeLeft.secondsLeft !== 0) {
                timeLeft.secondsLeft--;
            }
            else if (timeLeft.minutesLeft !== 0) {
                timeLeft.secondsLeft = 59;
                timeLeft.minutesLeft--;
            }
            else if (timeLeft.hoursLeft !== 0) {
                timeLeft.secondsLeft = 59;
                timeLeft.minutesLeft = 59;
                timeLeft.hoursLeft--;
            }
            else if (timeLeft.daysLeft !== 0) {
                timeLeft.secondsLeft = 59;
                timeLeft.minutesLeft = 59;
                timeLeft.hoursLeft = 23;
                timeLeft.daysLeft--;
            }
        }
        appClock.daysTag.html(appClock.timeLeft.daysLeft);
        appClock.hoursTag.html(appClock.timeLeft.hoursLeft);
        appClock.minutesTag.html(appClock.timeLeft.minutesLeft);
        appClock.secondsTag.html(appClock.timeLeft.secondsLeft);
    }
};

(function (win, doc, $) {
    $(win).on("load",function () {
        appClock.init();
    });
})(window, document, jQuery);